package com.alehno.icecreampower.application;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.application.adapter.CustomArrayAdapter;
import com.alehno.icecreampower.application.database.DatabaseHandler;
import com.alehno.icecreampower.application.dialog.DialogAdding;
import com.alehno.icecreampower.application.dialog.DialogChanging;
import com.alehno.icecreampower.application.dialog.DialogEditing;
import com.alehno.icecreampower.application.dialog.DialogUpdating;
import com.alehno.icecreampower.parser.IceCream;
import com.alehno.icecreampower.parser.SAXParser;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 18.07.2017.
 */

public class ListFragment extends Fragment implements FragmentInterface{

    private final String FILENAME = "info.xml";
    private CustomArrayAdapter adapter;
    private ListView listView;
    private EditText findText;
    private static SAXParser parser;
    private static IceCream iceCurrent = null;

    private DatabaseHandler db;
    DialogAdding dialogAdding;
    DialogChanging dialogChanging;
    DialogUpdating dialogUpdating;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db = MainActivity.getDb();
        dialogAdding = new DialogAdding();
        dialogChanging = new DialogChanging();
        dialogUpdating = new DialogUpdating();
        dialogUpdating.setMain(this);
        dialogChanging.setMain(this);
        dialogAdding.setFragment(this);

        parser = new SAXParser();
        List<IceCream> list = db.getAll();
        if(list.isEmpty()){
            list = parser.parse(FILENAME);
            for(IceCream iceCream : list){
                db.add(iceCream);
            }
        }
        List<String> listStr = new LinkedList<>();
        for(IceCream i: list){
            listStr.add(i.toString());
        }
        listView = (ListView) getActivity().findViewById(R.id.listView);
        adapter = new CustomArrayAdapter(getActivity(), listStr, list);
        listView.setAdapter(adapter);
        findText = (EditText) getActivity().findViewById(R.id.find);
        setListeners();
    }

    public void setListeners(){
        findText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String findStr = findText.getText().toString();
                List<IceCream> list = db.getAll(findStr);
                List<String> listStr = new LinkedList<String>();
                for(IceCream ice: list){
                    listStr.add(ice.toString());
                }
                adapter.setList(list);
                adapter.clear();
                adapter.addAll(listStr);
                listView.setAdapter(adapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        Button buttonFind = (Button) getActivity().findViewById(R.id.btnFind);
        buttonFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String findStr = findText.getText().toString();
                List<IceCream> list = db.getAll(findStr);
                List<String> listStr = new LinkedList<String>();
                for(IceCream i: list){
                    listStr.add(i.toString());
                }
                adapter.setList(list);
                adapter.clear();
                adapter.addAll(listStr);
                listView.setAdapter(adapter);
            }
        });
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogAdding.show(getActivity().getFragmentManager(), "dlgAdd");

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                iceCurrent = db.getList().get(position);
                if(iceCurrent.isEaten()){
                    Snackbar.make(view, "Ну хавала ты его уже, че ты", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else{
                    dialogChanging.show(getActivity().getFragmentManager(), "dlgChange");
                }
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                iceCurrent = db.getList().get(position);
                DialogEditing.setIceCream(iceCurrent);
                dialogUpdating.show(getActivity().getFragmentManager(), "dlgUpdating");
                return true;
            }
        });

        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void updateFragment(){
        SAXParser.setIceCreams(db.getAll());
        new SAXParser().writeObjects();
        String findStr = findText.getText().toString();
        List<String> listStr = new LinkedList<String>();
        List<IceCream> list = db.getAll(findStr);
        for(IceCream i: list){
            listStr.add(i.toString());
        }
        Parcelable state = listView.onSaveInstanceState();
        adapter.setList(list);
        adapter.clear();
        adapter.addAll(listStr);
        listView.setAdapter(adapter);
        listView.onRestoreInstanceState(state);
    }

    public void changeAsEaten(){
        iceCurrent.setEaten(true);
        db.update(iceCurrent);
        updateFragment();
    }

    public void deleteObject(){
        db.delete(iceCurrent);
        updateFragment();
    }

    public void editIceCream(IceCream iceCream){
        iceCream.setId(iceCurrent.getId());
        db.update(iceCream);
        updateFragment();
    }

    public void addObject(IceCream iceCream){
        db.add(iceCream);
        updateFragment();
    }

}
