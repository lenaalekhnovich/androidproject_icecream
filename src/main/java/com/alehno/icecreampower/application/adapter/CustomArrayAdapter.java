package com.alehno.icecreampower.application.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.parser.IceCream;

import java.util.List;

/**
 * Created by Босс on 18.06.2017.
 */

public class CustomArrayAdapter extends ArrayAdapter<String> {
    private Context context;
    private List<String> stringValues;
    private List<IceCream> list;

    public CustomArrayAdapter (Context context, List<String> stringValues,List<IceCream> list)
    {
        super(context, R.layout.list_item, stringValues);
        this.context = context;
        this.stringValues = stringValues;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_item, parent, false);
        TextView textView = (TextView) view.findViewById(R.id.colors);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.llColors);
        textView.setText(stringValues.get(position));
        IceCream iceCream = list.get(position);
        if (!iceCream.isEaten())
        {linearLayout.setBackgroundResource(R.color.red);
        }
        else   linearLayout.setBackgroundResource(R.color.white);
        return view;
    }



    public void setList(List<IceCream> list){
        this.list = list;
    }
}
