package com.alehno.icecreampower.application.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alehno.icecreampower.R;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageAdapter extends ArrayAdapter<Bitmap> {

    private int mGalleryItemBackground;
    private Context context;
    private Bitmap [] bitmapPic;

    public ImageAdapter(Context context, Bitmap[] bitmap) {
        super(context, R.layout.gallery_item);
        this.context = context;
        bitmapPic = bitmap;
    }

    public Bitmap[] getBitmapPic() {
        return bitmapPic;
    }

    public void setBitmapPic(Bitmap[] bitmapPic) {
        this.bitmapPic = bitmapPic;
    }

    @Override
    public int getCount() {
        return bitmapPic.length;
    }

    @Override
    public Bitmap getItem(int position) {
        return bitmapPic[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.gallery_item, parent, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        imageView.setImageBitmap(bitmapPic[position]);
        return view;
    }

    public Bitmap[] getFiles(){
        File directory = new File(Environment.getExternalStorageDirectory(),
                "IceCream/Photo");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File [] fList = directory.listFiles();
        Bitmap[] bitmaps = new Bitmap[fList.length + 1];
        for (int i = 0; i < fList.length; i++) {
            Bitmap bitmap = BitmapFactory.decodeFile(fList[i].getAbsolutePath());
            bitmaps[i] = bitmap;
        }
        return bitmaps;
    }
}
