package com.alehno.icecreampower.application;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.application.database.DatabaseHandler;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

/**
 * Created by Босс on 19.07.2017.
 */

public class DiagramFragment extends Fragment {
    private PieChart pieChart;
    private BarChart barChart;
    DatabaseHandler db;
    private static Activity main;

    public static Activity getMain() {
        return main;
    }

    public static void setMain(Activity main) {
        DiagramFragment.main = main;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_diagram, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db = MainActivity.getDb();
        pieChart = (PieChart) main.findViewById(R.id.diagram_all);
        barChart = (BarChart) main.findViewById(R.id.diagram_kind);
        createPieChart();
        createBarChart();

    }

    public void createPieChart(){
        int countAll = db.getIceCreamCount();
        int countEaten = db.getIceCreamCountEaten();
        int countNotEaten = countAll - countEaten;
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(countEaten, 0));
        entries.add(new Entry(countNotEaten, 1));

        pieChart.setHoleRadius(20);
        pieChart.setHoleColor(Color.argb(170,206,230,200));
        pieChart.setTransparentCircleRadius(25);
        PieDataSet dataSet = new PieDataSet(entries, "");
        final ArrayList<String> labels = new ArrayList<>();
        labels.add("Съеденные мороженки");
        labels.add("Несъеденные мороженки");
        dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(5);
        int [] colors = { Color.argb(255,255,215,64),Color.argb(255,190,183,255)};
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);

        PieData data = new PieData(labels, dataSet);
        data.setValueTextSize(12f);
        data.setValueTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
        data.setValueTextColor(Color.argb(252,100,100,100));
        pieChart.setData(data);

        pieChart.setDescription("Всего мороженок: " + countAll);
        pieChart.setDescriptionTextSize(14f);
        pieChart.setDescriptionTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
        pieChart.setDescriptionColor(Color.argb(252,100,100,100));
        pieChart.setDescriptionPosition(650,145);
        pieChart.animateY(5000);

        Legend l = pieChart.getLegend();
        l.setPosition(Legend.LegendPosition.LEFT_OF_CHART);
        l.setTextSize(14f);
        l.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
        l.setXEntrySpace(7);
        l.setYEntrySpace(3);
        l.setTextColor(Color.argb(252,100,100,100));

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                if (e == null)
                    return;

                Toast.makeText(getActivity(),
                        labels.get(e.getXIndex()) + ": " + e.getVal() + " шт.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    public void createBarChart(){
        ArrayList<BarEntry> entries = new ArrayList<>();
        int st = db.getCountByKind("Стаканчик");
        int pal = db.getCountByKind("Палочка");
        int rozh = db.getCountByKind("Рожок");
        int brik = db.getCountByKind("Брикет");
        entries.add(new BarEntry(st, 0));
        entries.add(new BarEntry(rozh, 1));
        entries.add(new BarEntry(pal, 2));
        entries.add(new BarEntry(brik, 3));
        entries.add(new BarEntry(db.getIceCreamCount() - st - pal - rozh - brik, 4));

        BarDataSet dataset = new BarDataSet(entries, "");

        final ArrayList<String> labels = new ArrayList<String>();
        labels.add("Cтаканчик");
        labels.add("Рожок");
        labels.add("Палочка");
        labels.add("Брикет");
        labels.add("Другое");
        dataset.setColors(ColorTemplate.VORDIPLOM_COLORS);
        dataset.setValueTextSize(12f);
        dataset.setValueTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));


        BarData data = new BarData(labels, dataset);
        data.setValueTextSize(12f);
        data.setValueTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
        barChart.setData(data);
        barChart.setDescription("");

        barChart.animateY(5000);

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                if (e == null)
                    return;

                Toast.makeText(getActivity(),
                        labels.get(e.getXIndex()) + ": " + e.getVal() + " шт.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }
}
