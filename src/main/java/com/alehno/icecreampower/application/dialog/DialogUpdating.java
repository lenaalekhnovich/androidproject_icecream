package com.alehno.icecreampower.application.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.application.ListFragment;

/**
 * Created by Босс on 20.06.2017.
 */

public class DialogUpdating extends DialogFragment implements DialogInterface.OnClickListener {
    private ListFragment main = null;


    public void setMain(ListFragment main) {
        this.main = main;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_action).setItems(R.array.action_array, this);
        return adb.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        int i = 0;
        switch (which) {
            case 0:
                DialogEditing dialogEditing = new DialogEditing();
                dialogEditing.setMain(main);
                dialogEditing.show(getFragmentManager(), "dlgEditing");
                break;
            case 1:
                AlertDialog.Builder adbDelete = new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.dialog_delete).setPositiveButton(R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        main.deleteObject();
                                    }
                                })
                        .setNeutralButton(R.string.cancel, this)
                        .setMessage(R.string.delete_data);
                adbDelete.show();
                break;
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
