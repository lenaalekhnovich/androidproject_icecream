package com.alehno.icecreampower.application.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.app.ListFragment;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.application.*;
import com.alehno.icecreampower.parser.IceCream;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 20.06.2017.
 */

public class DialogChanging extends DialogFragment implements DialogInterface.OnClickListener {

    private com.alehno.icecreampower.application.ListFragment main=null;


    public void setMain(com.alehno.icecreampower.application.ListFragment main) {
        this.main = main;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_save).setPositiveButton(R.string.yes, this)
                .setNeutralButton(R.string.cancel, this)
                .setMessage(R.string.save_data);
        return adb.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        int i = 0;
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                main.changeAsEaten();
                break;
            case Dialog.BUTTON_NEUTRAL:
                break;
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

}
