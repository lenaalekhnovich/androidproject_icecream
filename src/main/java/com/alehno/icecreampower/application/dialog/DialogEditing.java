package com.alehno.icecreampower.application.dialog;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.application.*;
import com.alehno.icecreampower.parser.IceCream;

/**
 * Created by Босс on 25.06.2017.
 */

public class DialogEditing  extends DialogFragment implements View.OnClickListener {

    static IceCream iceCream = null;
    EditText editTrademark;
    EditText editName;
    Spinner spinnerKind;
    Spinner spinner;
    private com.alehno.icecreampower.application.ListFragment main=null;

    public static IceCream getIceCream() {
        return iceCream;
    }

    public static void setIceCream(IceCream iceCream) {
        DialogEditing.iceCream = iceCream;
    }

    public void setMain(com.alehno.icecreampower.application.ListFragment main) {
        this.main = main;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Меняй меня полностью");
        View v = inflater.inflate(R.layout.dialog, null);
        v.findViewById(R.id.btnSave).setOnClickListener(this);
        v.findViewById(R.id.btnCancel).setOnClickListener(this);
        editTrademark = (EditText)v.findViewById(R.id.trademark);
        editTrademark.setText(iceCream.getTrademark());
        editName = (EditText)v.findViewById(R.id.name);
        editName.setText(iceCream.getName());
        spinnerKind = (Spinner)v.findViewById(R.id.spinner_kind);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        int position = KindEnum.valueOf(iceCream.getKind().trim().toUpperCase()).getNumber();
        spinnerKind.setSelection(position);
        spinner.setSelection(iceCream.isEaten() ? 0 : 1);
        //editKind = (EditText)v.findViewById(R.id.kind);
        //editKind.setText(iceCream.getKind());
        return v;
    }

    public void onClick(View v) {
        if(((Button)v).getText().toString().equals("Сохранить")){
            String trademark = editTrademark.getText().toString();
            String name = editName.getText().toString();
            String kind = spinnerKind.getSelectedItem().toString();
            //String kind = editKind.getText().toString();
            IceCream iceCream = new IceCream();
            iceCream.setEaten(false);
            iceCream.setTrademark(trademark);
            iceCream.setName(name);
            iceCream.setKind(kind);
            iceCream.setEaten(spinner.getSelectedItemPosition() == 0);
            main.editIceCream(iceCream);
            editTrademark.setText("");
            editName.setText("");
            //editKind.setText("");
        }
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}

