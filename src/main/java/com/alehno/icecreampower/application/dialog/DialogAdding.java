package com.alehno.icecreampower.application.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.application.FragmentInterface;
import com.alehno.icecreampower.parser.IceCream;

/**
 * Created by Босс on 20.06.2017.
 */

public class DialogAdding extends DialogFragment implements View.OnClickListener {

    EditText editTrademark;
    EditText editName;
    Spinner spinnerKind;
    Spinner spinner;
    private FragmentInterface fragment = null;


    public void setFragment(FragmentInterface main) {
        this.fragment = main;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Добавление мороженка");
        View v = inflater.inflate(R.layout.dialog, null);
        v.findViewById(R.id.btnSave).setOnClickListener(this);
        v.findViewById(R.id.btnCancel).setOnClickListener(this);
        editTrademark = (EditText)v.findViewById(R.id.trademark);
        editName = (EditText)v.findViewById(R.id.name);
        spinnerKind = (Spinner)v.findViewById(R.id.spinner_kind);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        return v;
    }

    public void onClick(View v) {
        if(((Button)v).getText().toString().equals("Сохранить")){
            String trademark = editTrademark.getText().toString();
            String name = editName.getText().toString();
            String kind = spinnerKind.getSelectedItem().toString();
            //String kind = editKind.getText().toString();
            IceCream iceCream = new IceCream();
            iceCream.setEaten(false);
            iceCream.setTrademark(trademark);
            iceCream.setName(name);
            iceCream.setKind(kind);
            iceCream.setEaten(spinner.getSelectedItemPosition() == 0);
            fragment.addObject(iceCream);
            editTrademark.setText("");
            editName.setText("");
            //editKind.setText("");

        }
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
