package com.alehno.icecreampower.application.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.application.FragmentInterface;

/**
 * Created by Босс on 19.07.2017.
 */

public class DialogUpdatingFile extends DialogFragment implements DialogInterface.OnClickListener {
    private FragmentInterface fragment = null;


    public void setFragment(FragmentInterface main) {
        this.fragment = main;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_action).setItems(R.array.action_file_array, this);
        return adb.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        int i = 0;
        switch (which) {
            case 0:
                DialogAdding dialogAdding = new DialogAdding();
                dialogAdding.setFragment(fragment);
                dialogAdding.show(getFragmentManager(), "dlgAdding");
                break;
            case 1:
                fragment.deleteObject();
                break;
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}

