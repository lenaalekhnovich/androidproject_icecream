package com.alehno.icecreampower.application;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.alehno.icecreampower.R;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageActivity extends AppCompatActivity {

    protected ImageView imageView;
    private static String path;
    private static Bitmap bitmap;

    public static String getPath() {
        return path;
    }

    public static void setPath(String pathS) {
        path = pathS;
    }

    public static Bitmap getBitmap() {
        return bitmap;
    }

    public static void setBitmap(Bitmap bitmapB) {
        bitmap = bitmapB;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        imageView = (ImageView) findViewById(R.id.imageViewPhoto);
        imageView.setBackgroundColor(Color.BLACK);
        imageView.setImageBitmap(bitmap);
        PhotoViewAttacher attacher = new PhotoViewAttacher(imageView);
        attacher.setZoomable(true);
    }
}
