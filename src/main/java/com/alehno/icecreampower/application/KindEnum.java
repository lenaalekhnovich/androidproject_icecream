package com.alehno.icecreampower.application;

/**
 * Created by Босс on 22.07.2017.
 */

public enum KindEnum {
    СТАКАНЧИК(0),
    ПАЛОЧКА(1),
    БРИКЕТ(2),
    РОЖОК(3),
    ТРУБОЧКА(4),
    БАТОНЧИК(5);

    KindEnum(int n){number = n;}

    int number;

    public int getNumber(){return number;}
}
