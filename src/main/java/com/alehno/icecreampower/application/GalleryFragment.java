package com.alehno.icecreampower.application;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.alehno.icecreampower.R;
import com.alehno.icecreampower.application.adapter.ImageAdapter;
import com.alehno.icecreampower.application.database.DatabaseHandler;
import com.alehno.icecreampower.application.dialog.DialogUpdatingFile;
import com.alehno.icecreampower.parser.IceCream;

import java.io.File;
import java.io.IOException;

/**
 * Created by Босс on 18.07.2017.
 */

public class GalleryFragment extends Fragment implements FragmentInterface{

    public static int currentPic = 0;
    private ListView listView;
    private ImageAdapter adapter;
    private DatabaseHandler db;
    DialogUpdatingFile dialogUpd;
    File[] files = null;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView = (ListView) getActivity().findViewById(R.id.gallery);
        adapter = new ImageAdapter(getContext(), getFiles());
        listView.setAdapter(adapter);
        dialogUpd = new DialogUpdatingFile();
        dialogUpd.setFragment(this);
        db = MainActivity.getDb();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                currentPic = position;
                ImageActivity.setBitmap(getFiles()[position]);
                Intent intent = new Intent(getActivity(), ImageActivity.class);
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                currentPic = i;
                dialogUpd.show(getActivity().getFragmentManager(), "dlUpd");
                return true;
            }
        });
    }


    @Override
    public void updateFragment() {
        try {
            Fragment fragment = (Fragment) ((GalleryFragment.class).newInstance());
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addObject(IceCream ice) {
        db.add(ice);
        deleteObject();
    }

    @Override
    public void deleteObject() {
        AlertDialog.Builder adbDelete = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_delete).setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                deleteFile();
                                updateFragment();
                            }
                        })
                .setNeutralButton(R.string.ne, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setMessage(R.string.dialog_delete_file);
        adbDelete.show();
    }

    public void deleteFile(){
        String path = files[currentPic].getAbsolutePath();
        File file = new File(path);
        if (file.exists()) {
            String deleteCmd = "rm -r " + path;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);

            } catch (IOException e) {
            }
        }

    }

    public Bitmap[] getFiles(){
        int width = getResources().getDimensionPixelSize(R.dimen.image_width);
        int height = getResources().getDimensionPixelSize(R.dimen.image_height);
        File directory = new File(Environment.getExternalStorageDirectory(),
                "IceCream/Photo");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File [] fList = directory.listFiles();
        files = fList;
        Bitmap[] bitmaps = new Bitmap[fList.length];
        for (int i = 0; i < fList.length; i++) {
            Bitmap bitmap = decodeSampledBitmapFromResource(fList[i].getAbsolutePath(), width, height);
            bitmaps[i] = bitmap;
        }
        return bitmaps;
    }


    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        return BitmapFactory.decodeFile(path, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

}
