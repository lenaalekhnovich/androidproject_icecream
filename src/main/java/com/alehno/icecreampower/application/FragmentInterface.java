package com.alehno.icecreampower.application;

import com.alehno.icecreampower.parser.IceCream;

/**
 * Created by Босс on 19.07.2017.
 */

public interface FragmentInterface {
    void updateFragment();
    void addObject(IceCream ice);
    void deleteObject();
}
