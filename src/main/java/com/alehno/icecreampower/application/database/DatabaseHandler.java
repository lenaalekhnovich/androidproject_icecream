package com.alehno.icecreampower.application.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.alehno.icecreampower.parser.IceCream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "iceCreamCatalog";
    private static final String TABLE = "iceCream";

    List<IceCream> list;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        list = new LinkedList<>();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "create table " + TABLE +
                "(id integer primary key autoincrement, " +
                "trademark text, name text, kind text, eaten integer);";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void add(IceCream iceCream) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("trademark", iceCream.getTrademark().trim());
        values.put("name", iceCream.getName().trim());
        values.put("kind", iceCream.getKind());
        values.put("eaten", iceCream.isEaten() ? 1 : 0);

        db.insert(TABLE, null, values);
        db.close();
    }


    public List<IceCream> getAll() {
        List<IceCream> iceCreamList = new LinkedList<IceCream>();
        String selectQuery = "SELECT  * FROM " + TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                IceCream iceCream = new IceCream();
                iceCream.setId(Integer.parseInt(cursor.getString(0)));
                iceCream.setTrademark(cursor.getString(1));
                iceCream.setName(cursor.getString(2));
                iceCream.setKind(cursor.getString(3));
                iceCream.setEaten(Integer.parseInt(cursor.getString(4)) == 1);
                iceCreamList.add(iceCream);
            } while (cursor.moveToNext());
        }
        Collections.sort(iceCreamList);
        list = iceCreamList;
        return iceCreamList;
    }

    public List<IceCream> getAll(String str){
        str = "%" + str.trim() + "%";
        String selection = "trademark like ? or name like ? or kind like ?";
        String[] selectionArgs = new String[] {str, str, str};
        List<IceCream> iceCreamList = new LinkedList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE, null, selection, selectionArgs, null, null,
                null);
        if (cursor.moveToFirst()) {
            do {
                IceCream iceCream = new IceCream();
                iceCream.setId(Integer.parseInt(cursor.getString(0)));
                iceCream.setTrademark(cursor.getString(1));
                iceCream.setName(cursor.getString(2));
                iceCream.setKind(cursor.getString(3));
                iceCream.setEaten(Integer.parseInt(cursor.getString(4)) == 1);
                iceCreamList.add(iceCream);
            } while (cursor.moveToNext());
        }
        Collections.sort(iceCreamList);
        list = iceCreamList;
        return iceCreamList;
    }

    public int getIceCreamCountEaten() {
        int count = 0;
        String eaten = "1";
        String selection = "eaten = ?";
        String[] selectionArgs = new String[] {eaten};
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE, null, selection, selectionArgs, null, null,
                null);
        if (cursor.moveToFirst()) {
            do {
                count++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return count;
    }

    public int getCountByKind(String kind){
        int count = 0;
        kind = kind + "%";
        String selection = "kind LIKE ?";
        String[] selectionArgs = new String[] {kind};
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE, null, selection, selectionArgs, null, null,
                null);
        if (cursor.moveToFirst()) {
            do {
                count++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return count;
    }

    public int update(IceCream iceCream) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("trademark", iceCream.getTrademark().trim());
        values.put("name", iceCream.getName().trim());
        values.put("kind", iceCream.getKind());
        values.put("eaten", iceCream.isEaten() ? 1 : 0);
        return db.update(TABLE, values, "id = ?",
                new String[] { String.valueOf(iceCream.getId())});
    }

    public void delete(IceCream iceCream) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE, "id = ?", new String[] { String.valueOf(iceCream.getId())});
        db.close();
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE, null, null);
        db.close();
    }


    public int getIceCreamCount() {
        List<IceCream> list = getAll();
        return list.size();
    }

    public List<IceCream> getList() {
        return list;
    }

    public void setList(List<IceCream> list) {
        this.list = list;
    }
}
