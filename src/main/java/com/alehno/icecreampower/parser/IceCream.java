package com.alehno.icecreampower.parser;

import android.support.annotation.NonNull;

/**
 * Created by Босс on 14.06.2017.
 */
public class IceCream implements Comparable<IceCream>{

    int id = 0;
    String trademark;
    String name;
    String kind;
    boolean eaten;

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public boolean isEaten() {
        return eaten;
    }

    public void setEaten(boolean eaten) {
        this.eaten = eaten;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return trademark + " - " + name + " (" + kind + ")";
    }


    @Override
    public int compareTo(@NonNull IceCream o) {
        IceCream ice = (IceCream) o;
        int v = this.getTrademark().compareTo(ice.getTrademark());
        if(v != 0 ){
            return v;
        }
        return this.getName().compareTo(ice.getName());
    }
}
