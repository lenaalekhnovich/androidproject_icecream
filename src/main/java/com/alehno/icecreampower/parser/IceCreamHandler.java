package com.alehno.icecreampower.parser;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 14.06.2017.
 */
public class IceCreamHandler extends DefaultHandler {

    private List<IceCream> iceCreams;
    private IceCream current = null;
    private IceCreamEnum currentElement = null;
    private StringBuilder currentStr;

    public IceCreamHandler(){
        iceCreams = new LinkedList<>();
    }

    public List<IceCream> getIceCreams(){
        return iceCreams;
    }

    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        switch (IceCreamEnum.valueOf(localName.toUpperCase())) {
            case ICECREAM:
                current = new IceCream();
                break;
            default:
                currentStr = new StringBuilder();
                break;

        }
        currentElement = IceCreamEnum.valueOf(localName.toUpperCase());
    }

    public void endElement(String uri, String localName, String qName) {
        switch (IceCreamEnum.valueOf(localName.toUpperCase())) {
            case ICECREAM:
                iceCreams.add(current);
                break;
            case TRADEMARK:
                current.setTrademark(currentStr.toString());
                break;
            case NAME:
                current.setName(currentStr.toString());
                break;
            case KIND:
                current.setKind(currentStr.toString());
                break;
            case EATEN:
                current.setEaten(Boolean.parseBoolean(currentStr.toString()));
                break;

        }
    }

    public void characters(char[] character, int start, int length) {
        String currentString = new String(character, start, length).trim();
        if (currentElement != null && !currentString.equals("")) {
            switch (currentElement) {
                case TRADEMARK:
                    currentStr.append(currentString);
                    break;
                case NAME:
                    currentStr.append(currentString);
                    break;
                case KIND:
                    currentStr.append(currentString);
                    break;
                case EATEN:
                    currentStr.append(currentString);
                    break;

            }
        }
    }
}
