package com.alehno.icecreampower.parser;

import android.os.Environment;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by Босс on 14.06.2017.
 */
public class SAXParser {

    private IceCreamHandler handler;
    private XMLReader reader;
    private static List<IceCream> iceCreams ;

    public SAXParser() {
        iceCreams = new LinkedList<>();
        handler = new IceCreamHandler();
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            javax.xml.parsers.SAXParser saxParser = saxParserFactory.newSAXParser();
            reader = saxParser.getXMLReader();
            reader.setContentHandler(handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void writeObjects()  {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        Document doc = null;
        try {
            doc = factory.newDocumentBuilder().newDocument();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Element root = doc.createElement("IceCreams");
        doc.appendChild(root);

        for(IceCream ice:iceCreams) {
            Element iceCream = doc.createElement("IceCream");
            root.appendChild(iceCream);

            Element trademark = doc.createElement("trademark");
            trademark.appendChild(doc.createTextNode(ice.getTrademark()));
            iceCream.appendChild(trademark);

            Element name = doc.createElement("name");
            name.appendChild(doc.createTextNode(ice.getName()));
            iceCream.appendChild(name);

            Element kind = doc.createElement("kind");
            kind.appendChild(doc.createTextNode(ice.getKind()));
            iceCream.appendChild(kind);

            Element eaten = doc.createElement("eaten");
            eaten.appendChild(doc.createTextNode(Boolean.valueOf(ice.isEaten()).toString()));
            iceCream.appendChild(eaten);
        }
        File sdPath = Environment.getExternalStorageDirectory();
        sdPath = new File(sdPath.getAbsolutePath() + "/" + "IceCream");
        File file = new File(sdPath,"info.xml");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(file));
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    public void parse(Reader readerIn) {
        try {
            reader.parse(new InputSource(readerIn));
        } catch (Exception e) {
           e.printStackTrace();
        }
        iceCreams = handler.getIceCreams();
        Collections.sort(iceCreams);
    }

    public List<IceCream> parse(String fileName) {
        List<IceCream> list = new LinkedList<>();
        File sdPath = Environment.getExternalStorageDirectory();
        sdPath = new File(sdPath.getAbsolutePath() + "/" + "IceCream");
        File sdFile = new File(sdPath, fileName);
        if(sdFile.exists()) {
            try {
                FileReader br = new FileReader(sdFile);
                parse(br);
                list = getIceCreams();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                sdFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public List<IceCream> getIceCreams() {
        return iceCreams;
    }

    public static void setIceCreams(List<IceCream> iceCreams) {
        SAXParser.iceCreams = iceCreams;
    }
}
